# Load modules and classes
package { 'emacs': }

if $::environment == 'dev' {
 exec { 'start-stack-dev':
   command   => 'docker-compose up --build -d',
   cwd       => '/vagrant',
   logoutput => true,
   provider  => shell,
   user      => 'vagrant',
 }
}
