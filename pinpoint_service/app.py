import connexion
from flask_cors import CORS

app = connexion.App(__name__)
CORS(app.app)

app.add_api('swagger.yml')
