from pyproj import Proj, transform
import logging

from pinpoint_service.grid_constants import GRID_CONSTANTS, PROJ_STRINGS

log = logging.getLogger(__name__)


class LocatorException(Exception):
    def __init__(self, arg):
        self.msg = arg


def column_row_from_lat_long(lat, lon, gpd_name=None):
    if not gpd_name:
        msg = 'No GPD_NAME specified for conversion'
        log.error(msg)
        raise LocatorException(msg)
    elif gpd_name not in GRID_CONSTANTS.keys():
        msg = 'Unsupported GPD_NAME "{}" specified'.format(gpd_name)
        log.error(msg)
        raise LocatorException(msg)

    grid = GRID_CONSTANTS[gpd_name]
    scale = grid['map_scale_m']

    eg_proj = Proj(PROJ_STRINGS[grid['projection']])
    wgs_proj = Proj(init='epsg:4326')

    x, y = transform(wgs_proj, eg_proj, lon, lat)

    i = grid['r0'] + x/scale
    j = grid['s0'] - y/scale

    return (i, j)
